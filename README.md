# Greekdev.Tutorials.Telemetry
This project is a tutorial about how to enable Telemetry for a .net application.


## Introduction

-What is open telemetry 


## Infrastructure Setup

### The Stack

In this tutorial, we will use the [Grafana stack](/docs/pages/grafana-stack.md). 

### Notice

This tutorial assumes **modern Windows** (10/11) with [**Podman Desktop**](https://podman-desktop.io/). 

If you are using docker desktop it should be sufficient to replace `podman` with `docker` on each command.

### Work folder

This tutorial contains infrastructure source code files. It is suggested to select/create a work folder and inside it create the following top-level folders

- `infrastructure`: Will contain any infrastructure-related files
- `src`: Will contain any source code files


We will setup our infrastructure using containers and we will need to reference some local files. 


### grafana

`\> podman run --name gdev-grafana -d -p 3000:3000 grafana/grafana`

### Verify installation

`\> start http://localhost:3000`  

![Grafana login image](./docs/images/grafana-login.jpg)

(default credentials are `admin/admin` and you will be asked to change them upon login)


### Prometheus

1. Create a file named `./prometheus/prometheus-config.yml`

2. Add the following [content](./infrastructure/prometheus/prometheus-config.yml) to `./prometheus/prometheus-config.yml`

3. Start the container `\> podman run --name gdev-prometheus -d -p 9090:9090 -v ./prometheus/prometheus-config.yml:/etc/prometheus/prometheus.yml prom/prometheus`

4. Verify the installation `\> start http://localhost:9090` or `\> start http://localhost:9090/metrics`

![Prometheus home screen](./docs/images/prometheus-homescreen.jpg)


### Loki

1. Create a file named `infra/loki/loki-config.yml`

2. Add the following [content](./infra/loki/loki-config.yml) to `infra/loki/loki-config.yml`

3. Start the container: `\> podman run --name gdev-loki -d -p 3100:3100 grafana/loki` 

4. Verify the installation `\> start http://localhost:3100/ready` and refresh until `ready` appears (may take up to 60 seconds)


**!! SCREENSHOT !!**


### Tempo

1. Create a file named `infra/tempo/tempo-config.yml`

2. Add the following [content](./infra/loki/loki-config.yml) to `infra/loki/loki-config.yml`

3. Start the container:
`\> podman run --name gdev-tempo -d -p 3200:3200 -v ./infra/tempo/tempo-config.yml:/etc/tempo-config.yml grafana/tempo "-config.file=/etc/tempo-config.yml"`

4. Verify the installation `\> start http://localhost:3200` or `\> start http://localhost:3200/metrics`

**!! SCREENSHOT !!**


### App

1. Create a new console app inside `src` using `dotnet new console --name GDev.Telemetry.ConsoleApp`

#### Logging Pipeline Setup
The best approach is to use *Serilog* for our logs because there is an already-implemented integration with **loki**.

2. Navigate to the application folder (in our case `cd GDev.Logging.ConsoleApp`) and install the required nuget packages


```
dotnet add package Serilog
dotnet add package Serilog.Settings.Configuration
dotnet add package Serilog.Sinks.Console
dotnet add package Serilog.Sinks.Grafana.Loki
```

3. Add the following [content](./src/GDev.Logging.ConsoleApp/Program.cs) to the `program.cs` of the console app.



### Tracing Pipeline Setup

```
dotnet add package OpenTelemetry 
dotnet add package OpenTelemetry.Exporter.OpenTelemetryProtocol
dotnet add package OpenTelemetry.Instrumentation.Http
```