#podman container rm gdev-grafana -f
podman container rm gdev-prometheus -f
podman container rm gdev-loki -f 
podman container rm gdev-tempo -f 

podman network rm gdev-network -f

podman network create gdev-network

podman run --network gdev-network --name gdev-grafana -d -p 3000:3000 grafana/grafana
podman run --network gdev-network --name gdev-prometheus -d -p 9090:9090 -v ./prometheus/prometheus-config.yml:/etc/prometheus/prometheus.yml prom/prometheus
podman run --network gdev-network --name gdev-loki -d -p 3100:3100 grafana/loki 
podman run --network gdev-network --name gdev-tempo -d -p 3200:3200 -p 4317:4317 -v ./tempo/tempo-config.yml:/etc/tempo-config.yml grafana/tempo "-config.file=/etc/tempo-config.yml"



start http://localhost:3000
start http://localhost:9090
start http://localhost:3100/ready
start http://localhost:3200/ready