﻿using Serilog;
using Serilog.Context;
using Serilog.Sinks.Grafana.Loki;

class Program
{
    const string facility = "gdev-tracing-consoleapp";

    static async Task Main(string[] args)
    {
        var label = new LokiLabel() { Key = "facility", Value = facility };

        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Override("Microsoft", Serilog.Events.LogEventLevel.Warning)
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .WriteTo.GrafanaLoki("http://localhost:3100", new List<LokiLabel>() { label })
            .CreateLogger();

        using (LogContext.PushProperty("action", "gdev-custom-action"))
        {
            Log.Logger.Information("Starting custom action");
            await Task.Delay(TimeSpan.FromMilliseconds(200));
            Log.Logger.Information("Custom action completed");
        }

        Log.CloseAndFlush();
    }
}
