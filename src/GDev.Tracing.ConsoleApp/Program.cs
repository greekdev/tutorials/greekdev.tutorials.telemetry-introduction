﻿using System.Diagnostics;
using OpenTelemetry;
using OpenTelemetry.Trace;

class Program
{
    // Create a "facility" label to group all our traces under
    const string facility = "gdev-logging-consoleapp";

    // This is the top-level activity that represents our application and generates all other activities
    private static ActivitySource _source = new ActivitySource(facility, "1.0.0");

    static async Task Main(string[] args)
    {
        // create a tracer provider (a destination for our traces to be send for storage)
        using var tracerProvider = Sdk.CreateTracerProviderBuilder()
            .AddSource(facility)
            .AddOtlpExporter(o => { o.Endpoint = new Uri("http://localhost:4317"); })
            .Build();

        // create a new activity (since there is no other activiy this is a top level activity)
        using (var activity = _source.StartActivity("User.Login"))
        {
            Console.WriteLine($"[{activity?.TraceId.ToHexString()}]:{activity?.DisplayName}");

            await Task.Delay(TimeSpan.FromMilliseconds(100));

            using (_source.StartActivity("Login"))
            {
                await Task.Delay(TimeSpan.FromMilliseconds(100));

                using (var localActicity = _source.StartActivity("usp_GetUserByUserName"))
                {
                    // invoke stored procedure
                    await Task.Delay(TimeSpan.FromMilliseconds(10));
                    localActicity?.SetStatus(ActivityStatusCode.Ok);
                }

                using (_source.StartActivity("ValidatePassword"))
                {
                    // Validate password
                    await Task.Delay(TimeSpan.FromMilliseconds(10));
                }

                using (_source.StartActivity("Get User Permissions"))
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(100));
                }
            }

            activity?.SetStatus(ActivityStatusCode.Ok);
        }
    }
}