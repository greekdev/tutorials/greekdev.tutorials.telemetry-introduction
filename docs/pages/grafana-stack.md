## The "Grafana Stack" for telemetry

* **Grafana** is an open-source analytics and interactive visualization web application. It provides charts, graphs, and alerts for the web when connected to supported data sources, enabling users to monitor and analyze metrics across various systems and applications.

* **Prometheus** is a monitoring and alerting toolkit. It features a multi-dimensional data model with time series data identified by metric name and key/value pairs, primarily focused on reliability and scalability, supporting various modes of graphing and dashboarding.


* **Loki** is a log aggregation system inspired by Prometheus. Unlike traditional logging systems, Loki is designed to efficiently store and query logs from any environment, focusing on simplicity and cost-effectiveness.


* **Tempo** is a distributed tracing backend. It stores and aggregates tracing data, allowing users to query and visualize distributed traces, helping to understand the flow and performance of distributed systems.